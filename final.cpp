#include<iostream>
#include<string>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<Windows.h>

using namespace std;
//define globals
FILE *courseFile;
FILE *studentFile;
int filled_elements_c= 0;
int filled_elements_s=0;
struct lesson
{
	string code;
	string name;
	int unit;
	string teacher;
	struct lesson *next;
	struct lesson *previous;
};
struct std_lesson
{
	struct lesson *course;
	float grade;
	struct std_lesson *next;
	struct std_lesson *previous;
};
struct student
{
	string first_name;
	string last_name;
	string student_number;
	int taken;
	int vahed;
	float avg;
	struct std_lesson *lesson;
	struct student *previous;
	struct student *next;
};
struct student *firstS = NULL ,*CurStudent = NULL;
struct std_lesson *CurStudentCourse = NULL;
struct lesson *firstC = NULL, *CurCourse = NULL;
void read();//reads from file
void write_s();//write lessons on file
void write_c();//write courses on file
void main_menu();
void remove_student();
void add_new_student();// case 1
void add_new_lesson();//case 3
int search_s(char str[]);//search in students
int search_C(char str[]);//search in courses
void remove_student();
void add_lesson_for_student();//case 2
void sort_last_name();
void sort_std_number();
void print ();//display info
void top_three();
void student_information_show(); //case 10
void LessThan_14_show ();
void failed_students();
void student_information_show();
int main()
{
	system("color 3F");//background color
	read();
	main_menu();
	write_c();
	write_s();
	return 0;
}
void main_menu()
{
	int choice; // define a variable to save user's choice
	int flag=1;
	while (flag==1)
	{
		system("cls");
		cout<<"\n\n\n\n\t\t\t\t\t\tEnterance: "<<__TIME__<<" | "<<__DATE__;
        cout<<"\n\n\t\t\t\t\t\t========================< MAIN MENU >========================\n";
        cout<<"\n\t\t\t\t\t\t1. Add a new student";
        cout<<"\n\t\t\t\t\t\t2. Add a lesson for a student";
        cout<<"\n\t\t\t\t\t\t3. Add a new lesson";
        cout<<"\n\t\t\t\t\t\t4. Show students information  ( BASED ON LAST NAME)";
        cout<<"\n\t\t\t\t\t\t5. Show students information  ( BASED ON STUDENT'S NUMBER)";
        cout<<"\n\t\t\t\t\t\t6. Show	failed students information";
        cout<<"\n\t\t\t\t\t\t7. Show top 3 students information";
        cout<<"\n\t\t\t\t\t\t8. Show students information who has passed less than 14 courses";
        cout<<"\n\t\t\t\t\t\t9. Remove a student";
        cout<<"\n\t\t\t\t\t\t10.Show a student information";
        cout<<"\n\t\t\t\t\t\t11.Remove a lesson";
        cout<<"\n\t\t\t\t\t\t12.Exit\n\n";
        cout<<"\t\t\t\t\t\t=============================================================\n";
		cout<<"\n\n\t\t\t\t\t\tEnter your choice : ( a number between 1 to 12)\n\n";
		cout<<"\t\t\t\t\t\t  >> ";
		cin >>choice;
		switch(choice)
		{
			case 1 :
				system("cls");

				add_new_student();
				write_c();
				write_s();
				break;
			case 2:
				system("cls");

				add_lesson_for_student();
				write_c();
				break;
			case 3:
						system("cls");

				add_new_lesson();
				write_c();
				write_s();
				break;
			case 4 :
						system("cls");

				sort_last_name();
				write_c();
				write_s();
				break;
			case 5:
						system("cls");

				sort_std_number();
				write_c();
				write_s();
				break;
			case 6:
						system("cls");

                failed_students();
				break;
			case 7:
						system("cls");

                top_three();
				break;
			case 8:
						system("cls");

				LessThan_14_show ();
				break;
			case 9:
						system("cls");

				remove_student();
				break;
			case 10:
						system("cls");

			    student_information_show();
				break;
			case 11:
				//remove_lesson();
				break;
            case 12:
                flag=0;
                return;
				break;
			default:
				cout<<"\n\n\t\t\t\t\t\t [ERROR]   WRONG ENTRY !!!";
				Sleep(5000);
	}//switch
    }//while
}//main_menu
void read()
{
	int  i , j;
	studentFile = fopen("student.txt", "r+");
	struct student *input_student;
	struct std_lesson *tempSC;
	if (studentFile == NULL)
	{
		write_s();
		write_c();
	}
	fscanf(studentFile,"%d\n",&filled_elements_s);
	for (i = 0; i<filled_elements_s; i++)
	{
		if (i == 0)
		{
			firstS = new (struct student);
			firstS->lesson = NULL;
			firstS->previous = firstS;
			firstS->next = firstS;
			fscanf(studentFile, "%s\t%s\t%s\t%d\t%f\n", firstS->student_number.c_str(), firstS->first_name.c_str(), firstS->last_name.c_str(), &(firstS->taken),&(firstS->avg));
			for (j = 0; j < (firstS->taken); j++)
			{
				if (j == 0)
				{
					firstS->lesson = new (struct std_lesson);
					firstS->lesson->previous = firstS->lesson;
					firstS->lesson->next = firstS->lesson;
					fscanf(studentFile, "%s\t%f\n", firstS->lesson->course->code.c_str(), &(firstS->lesson->grade));
					CurStudentCourse = firstS->lesson;
				}
				else
				{
					tempSC = new (struct std_lesson);
					tempSC->next = firstS->lesson;
					firstS->lesson->previous = tempSC;
					tempSC->previous = CurStudentCourse;
					CurStudentCourse->next = tempSC;
					fscanf(studentFile, "%s\t%f\n", tempSC->course->code.c_str(), &(tempSC->grade));
					CurStudentCourse = tempSC;
				}
			}
			CurStudent = firstS;
		}
		else
		{
			input_student = new (struct student);
			input_student->next = firstS;
			firstS->previous = input_student;
			input_student->previous = CurStudent;
			CurStudent->next = input_student;
			input_student->lesson = NULL;
			fscanf(studentFile, "%s\t%s\t%s\t%d\n", input_student->student_number.c_str(), input_student->first_name.c_str(), input_student->last_name.c_str(), &(input_student->taken));
			for (j = 0; j < (input_student->taken); j++)
			{
				if (j == 0)
				{
					input_student->lesson = new (struct std_lesson);
					input_student->lesson->previous = input_student->lesson;
					input_student->lesson->next = input_student->lesson;
					fscanf(studentFile, "%s\t%f\n", input_student->lesson->course->code.c_str(), &(input_student->lesson->grade));
					CurStudentCourse = input_student->lesson;
				}
				else
				{
					tempSC = new (struct std_lesson);
					tempSC->next = input_student->lesson;
					input_student->lesson->previous = tempSC;
					tempSC->previous = CurStudentCourse;
					CurStudentCourse->next = tempSC;
					fscanf(studentFile, "%s\t%f\n", tempSC->course->code.c_str(), &(tempSC->grade));
					CurStudentCourse = tempSC;
				}
			}
			CurStudent = input_student;
		}
	}
	filled_elements_s=i;
	fclose(studentFile);
	courseFile = fopen("course.txt", "r");
	struct lesson *CItemp;
	if (courseFile == NULL)
	{
	fprintf(stderr, "%s\n", "  Unable to open the file");
	exit(0);
	}
	fscanf(courseFile,"%d\n",&filled_elements_c);
	for (i = 0; i<filled_elements_c; i++)
	{
	if (i == 0)
	{
	firstC = new (struct lesson);
	firstC->next = firstC;
	firstC->previous = firstC;
	fscanf(courseFile, "%s\t%s\t%d\t%s\n", firstC->code.c_str(), firstC->name.c_str(), &(firstC->unit), firstC->teacher.c_str());
	CurCourse = firstC;
	}
	else
	{
	CItemp = new (struct lesson);
	CItemp->next = firstC;
	firstC->previous = CItemp;
	CItemp->previous = CurCourse;
	CurCourse->next = CItemp;
	fscanf(courseFile, "%s\t%s\t%d\t%s\n", CItemp->code.c_str(), CItemp->name.c_str(),&( CItemp->unit), CItemp->teacher.c_str());
	CurCourse = CItemp;
	}
	}
	filled_elements_c = i;
	fclose(courseFile);
}

void write_s()
{
	int n, i, j;
	studentFile = fopen("student.txt", "w");
	if (studentFile == NULL)
	{
		fprintf(stderr, "%s\n", "  Unable to open the file");
		exit(0);
	}
	fprintf(studentFile,"%d\n",filled_elements_s);
	for (i = 0, CurStudent = firstS; i<filled_elements_s; i++, CurStudent = CurStudent->next)
	{
		fprintf(studentFile, "%s\t%s\t%s\t%d\t%f\n", CurStudent->student_number.c_str(), CurStudent->first_name.c_str(), CurStudent->last_name.c_str(), CurStudent->taken,CurStudent->avg);
		for (j = 0, CurStudentCourse = CurStudent->lesson; j<(CurStudent->taken); j++, CurStudentCourse = CurStudentCourse->next)
			fprintf(studentFile, "%s\t%f\n", CurStudentCourse->course->code.c_str(), CurStudentCourse->grade);
	}
	fclose(studentFile);
}

void write_c()
{
	int n,i,j;
	courseFile = fopen("course.txt", "w");
	if (courseFile == NULL)
	{
	fprintf(stderr, "%s\n", "  Unable to open the file");
	exit(0);
	}
	fprintf(courseFile,"%d\n",filled_elements_c);
	for (i = 0, CurCourse = firstC; i < filled_elements_c; i++, CurCourse = CurCourse->next)
	{
	fprintf(courseFile, "%s\t%s\t%d\t%s\n", CurCourse->code.c_str(), CurCourse->name.c_str(), CurCourse->unit, CurCourse->teacher.c_str());
	}
	fclose(courseFile);
}

int search_C(string str)
{
	int counter;
	for (counter = 0, CurCourse = firstC; firstC != NULL && counter < filled_elements_c; counter++, CurCourse = CurCourse->next)
	{
		if (strcmp(CurCourse->code.c_str(),str.c_str())==0)
			return 1;
	}
	return 0;
}

void add_lesson_for_student()
{
	float grade=0;
	string str;
	struct std_lesson * temp;
	struct student *CurStudent;
	struct lesson *CurCourse;
	temp=new struct std_lesson;
	temp->course=new lesson;
	int counter_s=0;
	int counter_c=0;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	cout<<"\n\n\t\t\t\t\t\t======================< ADD LESSON FOR STUDENT >======================\n";
	cout<<"\n\t\t\t\t\t\tEnter student number:\n\t\t\t\t\t  >> ";
	cin >> str;
	int found=0;
	for (counter_s=0,CurStudent = firstS;counter_s<filled_elements_s && CurStudent!= NULL;counter_s++,CurStudent = CurStudent->next)
	{
		if (strcmp(CurStudent->student_number.c_str(),str.c_str())==0)
		{
		    found=1;
            int flag=1;
            while(flag)
            {
                cout<<"\n\t\t\t\t\t\tEnter lesson code:\n\t\t\t\t\t  >> ";
                cin >> temp->course->code;
                for (counter_c=0,CurCourse = firstC;counter_c<filled_elements_c && CurCourse!= NULL;counter_c++, CurCourse = CurCourse->next)
                {
                    if(strcmp(CurCourse->code.c_str(),temp->course->code.c_str())==0)
                    {
                        flag=0;
                        cout<<"\n\t\t\t\t\t\tEnter lesson grade:\n\t\t\t\t\t  >> ";
                        cin >> temp->grade;
                        grade=temp->grade;
                        temp->course->name=CurCourse->name;
                        temp->course->teacher=CurCourse -> teacher;
                        temp->course->unit=CurCourse->unit;

					if (CurStudent->lesson==NULL)
					{
						CurStudent->lesson=temp;
						CurStudent->lesson->next=temp;
						CurStudent-> lesson->previous=temp;
						CurStudent->vahed = CurCourse->unit;
						CurStudent->avg=grade;
						CurStudent->taken=1;
					}
					else
					{
						temp->next=CurStudent->lesson;
						CurStudent->lesson=temp;
						int pre_vahed=CurStudent->vahed;
						CurStudent->vahed+=CurCourse->unit;
						CurStudent->avg=((CurStudent->avg)*pre_vahed + grade*(CurCourse->unit))/CurStudent->vahed;
						CurStudent->taken++;
					}

				}//if code
			}//for
			}//while
		}//if curstudent
	}//for curstudent
	if (found==0)
    {
        cout << "\t\t\t\t\t\t\t[ERROR]   No Such a student!!!" <<endl;
    }
}

int search_s(string str)
{
	int counter;
	for (counter = 0, CurStudent = firstS; firstS != NULL && counter < filled_elements_s; counter++, CurStudent = CurStudent->next)
	{
		if (strcmp(CurStudent->student_number.c_str(),str.c_str())==0)
			return 1;
	}
	return 0;
}

void add_new_student()
{
	struct student *Stemp;
	Stemp = new (struct student);
	cout << "\n\t\t\t\t\t\t=======================< Add Student >=======================\n\n";
	cout << "\n\t\t\t\t\t\tEnter student number : (exactly6 digits)\n\n\t\t\t\t\t\t >";
	cin >> Stemp->student_number;
	getchar();
	if (search_s(Stemp->student_number) == 1)
	{
		cout << "\n\t\t\t\t\t\t [ERROR]   student number is already taken!!! ";
		Sleep(5000);
		free(Stemp);
		return;
	}
	cout << "\n\t\t\t\t\t\tEnter student first name :\n\n\t\t\t\t\t\t > ";
	cin >> Stemp -> first_name;
	getchar();
	cout << "\n\t\t\t\t\t\tEnter student last name :\n\n\t\t\t\t\t\t > ";
	cin>>Stemp->last_name;
	Stemp->taken = 0;
	if (firstS == NULL)
	{
		Stemp->next = Stemp;
		Stemp->previous = Stemp;
		Stemp->lesson = NULL;
		firstS = CurStudent = Stemp;
	}
	else
	{
		Stemp->next = CurStudent;
		Stemp->previous = CurStudent->previous;
		CurStudent->previous = Stemp;
		Stemp->previous->next = Stemp;
		Stemp->lesson = NULL;
	}
	filled_elements_s++;
	write_s();
}
void add_new_lesson()
{
	struct lesson *ctemp;
	ctemp = new (struct lesson);
	cout << "\n\t\t\t\t\t\t=======================< Add lesson >=======================\n\n";
	cout << "\n\t\t\t\t\t\tenter lesson number : (at most 7 digits)\n\n\t\t\t\t\t\t >>";
	cin >> ctemp->code;
	getchar();
	if (search_C(ctemp->code) ==1)
	{
			cout << "\n\t\t\t\t\t\t [ERROR]   lesson already exists !!! ";
		free(ctemp);
		return;
	}
	cout << "\n\t\t\t\t\t\tEnter lesson name :\n\n\t\t\t\t\t\t >> ";
	cin >> ctemp->name;
	getchar();
	cout << "\n\t\t\t\t\t\tEnter course units :\n\n\t\t\t\t\t\t >> ";
	cin>> ctemp->unit;
	cout << "\n\t\t\t\t\t\tEnter teacher name :\n\n\t\t\t\t\t\t >> ";
	cin >> ctemp->teacher;
	getchar();
	if (firstC == NULL)
	{
		ctemp->next = ctemp;
		ctemp->previous = ctemp;
		firstC = CurCourse = ctemp;
	}
	else
	{
		ctemp->next = CurCourse;
		ctemp->previous = CurCourse->previous;
		CurCourse->previous = ctemp;
		ctemp->previous->next = ctemp;
	}

	filled_elements_c++;
	write_c();
  }

void sort_last_name ()
{
	cout<<"\n\n\t\t\t\t\t\t=====================< SORT BASED ON LAST NAME >=====================\n";
    int i=0;
    struct student  *pre=NULL ,*second;
    int swaped=1;
    int n=filled_elements_s;
    while (swaped)
    {
        swaped=0;
        pre=NULL;
        CurStudent=firstS;
        i=0;
        while (CurStudent->next->next!=NULL && i<n)
        {
            second=CurStudent->next;
            if (strcmp(CurStudent->last_name.c_str(),CurStudent->next->last_name.c_str())>0)
            {
                if(pre==NULL)
                {
                    firstS=second;
                }
                else
                {
                    pre->next=second;
                }
                CurStudent->next=second->next;
                second->next=CurStudent;
                pre=second;
                swaped=1;
            }
            else
            {
                pre=CurStudent;
                CurStudent=CurStudent->next;
            }
            i++;
        }
        n--;
    }
    print();
}

void top_three ()
{
    int i=0;
    struct student  *pre=NULL ,*second;
    int swaped=1;
    int n=filled_elements_s;
    while (swaped)
    {
        swaped=0;
        pre=NULL;
        CurStudent=firstS;
        i=0;
        while (CurStudent->next->next!=NULL && i<n)
        {
            second=CurStudent->next;
            if (CurStudent->avg < CurStudent->next->avg)
            {
                if(pre==NULL)
                {
                    firstS=second;
                }
                else
                {
                    pre->next=second;
                }
                CurStudent->next=second->next;
                second->next=CurStudent;
                pre=second;
                swaped=1;
            }
            else
            {
                pre=CurStudent;
                CurStudent=CurStudent->next;
            }
            i++;
        }
        n--;
    }
    int j=0;
    struct student *student_j;
	cout << "\n\t\t\t\t\t\t======================< TOP 3 STUDENTS >======================\n\n";
    for (j=0,student_j=firstS;j<3;j++,student_j=student_j->next)
    {
        cout <<"\n\t\t\t\t\t\t"<< student_j->first_name<<"\t";
        cout << student_j->last_name << "\t";
        cout << student_j->student_number << "\t";
        cout << student_j->avg;
		cout << endl;
    }

}

void sort_std_number ()
{
	cout<<"\n\n\t\t\t\t\t\t===================< SORT BASED ON STUDENT NUMBER>===================\n";
    int i=0;
    struct student  *pre=NULL ,*second;
    int swaped=1;
    int n=filled_elements_s;
    while (swaped)
    {
        swaped=0;
        pre=NULL;
        CurStudent=firstS;
        i=0;
        while (CurStudent->next->next!=NULL && i<n)
        {
            second=CurStudent->next;
            //print ();
            if (strcmp(CurStudent->student_number.c_str(),CurStudent->next->student_number.c_str())>0)
            {
                if(pre==NULL)
                {
                    firstS=second;
                }
                else
                {
                    pre->next=second;
                }
                CurStudent->next=second->next;
                second->next=CurStudent;
                pre=second;
                swaped=1;
            }
            else
            {
                pre=CurStudent;
                CurStudent=CurStudent->next;
            }
            i++;
        }
        n--;
    }
    print();

}

void print ()
{
    int i=0;
    struct student *student_i;
    for (i=0,student_i=firstS;i<filled_elements_s;i++,student_i=student_i->next)
    {
		cout<<"\n\t\t\t\t\t\t";
        cout << student_i->first_name.c_str() << "\t";
        cout << student_i->last_name.c_str() << "\t";
        cout << student_i->student_number.c_str() << "\t";
        cout << student_i->avg;
    }
}


void LessThan_14_show ()
{
	cout<<"\n\n\t\t\t\t\t\t======================<LESS THAN 14 UNITS >======================\n";
    int i=0;
    struct student *current;
    for (i=0,current=firstS;i<filled_elements_s;i++,current=current->next)
    {
        if (current->vahed<14)
        {
			cout<<"\n\t\t\t\t\t\t";
            cout << current->first_name << "\t";
            cout << current->last_name << "\t";
            cout << current->student_number <<"\t";
            cout << current->taken;
        }
    }

}
void student_information_show()
{
	cout<<"\n\n\t\t\t\t\t\t======================< STUDENT INFORMATION >======================\n";
	struct student *tempb;
	struct std_lesson *CurStudentCourse;
	int flag = 0;
	int counter_s=0;
	string numstd;
	cout << "\n\t\t\t\t\t\t Enter student number.\n\t\t\t\t\t\t\t>> ";
	cin >> numstd;
	cout <<"\n\t\t\t\t\t\t\t\t\t::: INFORMATION :::\n\n";
	for (counter_s = 0, tempb = firstS; tempb!= NULL && counter_s < filled_elements_s;tempb=tempb->next,counter_s++)
	{
		if (strcmp(numstd.c_str() , tempb->student_number.c_str())==0)
		{
			flag = 1;
			cout << "\t\t\t\t\t\t\tFIRST NAME : " << tempb->first_name << "\n";
			cout << "\t\t\t\t\t\t\tLAST NAME : " << tempb->last_name << "\n";
			cout << "\t\t\t\t\t\t\tSTUDENT NUMBER : " << tempb->student_number << "\n";
			cout << "\t\t\t\t\t\t\tAVERAGE : " << tempb->avg << "\n";
			
			}
		}
	if(flag==0)
		cout<<"\n\t\t\t\t\t\t[ERROR]   No such student!!!";
	}
	

void failed_students()
{
    int i=0;
    struct student *current;
    for (i=0,current=firstS;i<filled_elements_s;i++,current=current->next)
    {
        if (current->avg<12)
        {
            cout<<current->first_name<<"\t" ;
            cout << current->last_name << "\t" ;
            cout <<current->student_number<<"\t";
            cout <<current->avg <<endl;

        }
    }
}
void remove_student()
{
	string str;
	int counter;
//	struct student * next;
	struct student *temp;
	cout << "Enter student number:";
	cin >> str;
//	getchar();

	for (counter = 0, CurStudent = firstS ; firstS != NULL && counter < filled_elements_s; counter++, CurStudent = CurStudent->next)
	{
		if (strcmp(CurStudent->student_number.c_str(), str.c_str()) == 0)
		{
			temp = CurStudent;
			CurStudent->next->previous = CurStudent->previous;
			CurStudent->previous->next = CurStudent->next;
			delete temp;
			if (filled_elements_s == 1)
				firstS = NULL;
			filled_elements_s--;
			break;
		}
	}
}